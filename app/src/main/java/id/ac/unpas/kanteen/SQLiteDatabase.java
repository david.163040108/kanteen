package id.ac.unpas.kanteen;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

public class SQLiteDatabase extends SQLiteOpenHelper
{
    public SQLiteDatabase(Context context, String name, android.database.sqlite.SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void queryData(String sql){
        android.database.sqlite.SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

    public void insertData(String nama, String harga, byte[] gambar){
        android.database.sqlite.SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO Makanan VALUES (NULL, ?, ?, ?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, nama);
        statement.bindString(2, harga);
        statement.bindBlob(3, gambar);

        statement.executeInsert();
    }

    public void updateData(String nama, String harga, byte[] gambar, int id) {
        android.database.sqlite.SQLiteDatabase database = getWritableDatabase();

        String sql = "UPDATE Makanan SET nama = ?, harga = ?, gambar = ? WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);

        statement.bindString(1, nama);
        statement.bindString(2, harga);
        statement.bindBlob(3, gambar);
        statement.bindDouble(4, (double)id);

        statement.execute();
        database.close();
    }

    public  void deleteData(int id) {
        android.database.sqlite.SQLiteDatabase database = getWritableDatabase();

        String sql = "DELETE FROM Makanan WHERE id = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        database.close();
    }

    public Cursor getData(String sql){
        android.database.sqlite.SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }





    @Override
    public void onCreate(android.database.sqlite.SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(android.database.sqlite.SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
